import React from 'react';
import ShowProduct from './components/ShowProduct/ShowProduct';
import './App.css';
import EcomApp from './components/EcomApp/EcomApp';
import "bootstrap"

class App extends React.Component {
  render() {
    return (
      <>
        <EcomApp />
      </>
    )
  }
}


export default App;
