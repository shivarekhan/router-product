import React from "react";
import "./Navbar.css"
import { Link } from "react-router-dom";

class Navbar extends React.Component {
    render() {
        return (
            <>
                <div className="nav-parent">
                    <h1 className="nav-logo"><Link to="/"><i className="fa-sharp fa-solid fa-house white-color "></i></Link ></h1>
                    <div className="nav-list">
                        <ul className="nav-list-parent">
                            <Link to="/cart"><i className="fa-solid fa-cart-shopping fa-3x white-color"></i></Link >
                        </ul>
                    </div>
                </div>
            </>
        )
    }
}

export default Navbar;