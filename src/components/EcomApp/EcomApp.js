import React, { Component } from 'react'
import axios from 'axios';
import { BrowserRouter, Route, Switch  } from 'react-router-dom';
import Navbar from '../Navbar/Navbar';
import ShowProduct from '../ShowProduct/ShowProduct';
import "./EcomApp.css"
import ProductPage from '../ProductPage/ProductPage';
import Cart from '../Cart/Cart';


class EcomApp extends Component {
    constructor(props) {
        super(props);
        this.API_STATES = {
            LOADING: "loading",
            ERROR: "error",
            LOADED: "loaded",

        }
        this.state = {
            fetchedProducts: [],
            status: this.API_STATES.LOADING,
            errorMsg: "",
        };
        this.API_ENDPOINT = "https://fakestoreapi.com/products";
    }

    fetchedProductsViaApi = () => {

        axios.get(this.API_ENDPOINT)
            .then((response) => {
                if (response.data.length === 0) {
                    this.setState({
                        failedMsg: "!! NO product to Show.",
                        status: this.API_STATES.LOADED,
                    })

                } else {
                    const fetchedProducts = response.data;
                    this.setState({
                        fetchedProducts,
                        status: this.API_STATES.LOADED
                    });
                }
            })
            .catch((error) => {
                this.setState({
                    status: this.API_STATES.ERROR,
                    failedMsg: "Api fetch failed Please try after some time.",

                })
            })

    }
    render() {
        return (
            <>
                <div className='parent'>
                    <BrowserRouter>
                        <Navbar />
                        <Switch>
                            <Route exact path="/">
                                <ShowProduct
                                    fetchedProducts={this.state.fetchedProducts}
                                    status={this.state.status}
                                    errorMsg={this.state.errorMsg}
                                />
                            </Route>
                            <Route exact path="/product/:id" render={(props) => {
                                return <ProductPage
                                    {...props}
                                    products={this.state.fetchedProducts} />
                            }} />
                            <Route exact path="/cart">
                                <Cart
                                    products={this.state.fetchedProducts} />
                            </Route>
                        </Switch>
                    </BrowserRouter>
                </div>
            </>
        )
    }

    componentDidMount() {
        this.setState({
            status: this.API_STATES.LOADING,
        }, this.fetchedProductsViaApi);
    }
}

export default EcomApp;