import React, { Component } from 'react'
import axios from 'axios'
import Loader from '../Loader/Loader'
import CartList from '../CartList/CartList'

export default class Cart extends Component {
    constructor(props) {
        super(props)
        this.API_STATES = {
            LOADING: "loading",
            ERROR: "error",
            LOADED: "loaded",
        }
        this.state = {
            cartProducts: [],
            status: this.API_STATES.LOADING,
        }
    }
    render() {

        return (
            <>


                {this.state.status === "loading" && <Loader />}
                {this.state.status === "error" && <h1>No products in cart</h1>}
                {this.state.status === "loaded" && <div className='parent'>
                    {Object.entries(this.state.cartProducts).map((current) => {
                        return (<CartList
                            key={current[1].id}
                            title={current[1].title}
                            image={current[1].image}
                            price={current[1].price}
                            quantity={current[1].quantityOfProduct}
                        />)
                    })}
                </div>}
            </>
        )
    }

    componentDidMount() {


        axios.get("https://fakestoreapi.com/carts/user/1")
            .then((response) => {
                if (response.data["0"].products.length >= 0) {
                    let receivedData = response.data["0"].products;
                    let productIdAndCount = receivedData.reduce((acc, currentObject, index) => {
                        const productId = currentObject.productId;
                        const quantity = currentObject.quantity;
                        const product = { ...this.props.products[productId], quantityOfProduct: quantity }
                        acc[productId] = { ...product };
                        return acc;

                    }, {})

                    this.setState({
                        cartProducts: productIdAndCount,
                        status: this.API_STATES.LOADED,
                    })
                }
            })

    }
}


