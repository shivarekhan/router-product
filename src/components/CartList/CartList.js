import React from 'react'
import "./CartList.css";

function CartList(props) {
    let { title, image, price, quantity } = props
    const finalPrice = Number(price) * Number(quantity);
    return (
        <>
            <div className='parent-cartlist'>
                <div className='image-cart'>
                    <img src={image} />
                </div>
                <div className='details-box'>

                    <div className='title'>
                        <h1>Title - {title}</h1>
                    </div>
                    <div className='cartlist-price'>
                        <h2>Price -${price}</h2>
                    </div>
                    <div className='quantity'>
                        <h2>Quantity - {quantity}</h2>
                    </div>
                    <div className='final Amount'><h2>final amount - ${finalPrice}</h2></div>
                </div>

            </div>
        </>
    )
}

export default CartList;