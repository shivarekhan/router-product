import React, { Component } from 'react'
import "./ProductPage.css"


class ProductPage extends Component {
    render() {
        return (
            <>
                {this.props.match.params.id > 20 &&
                    <h1>Product not found</h1>}

                {this.props.match.params.id <= 20 &&
                    < div className='single-product-page'>
                        <div className='image-container-page'>
                            <img src={this.props.products[this.props.match.params.id - 1].image} alt="product" />
                        </div>

                        <div className='details-page'>
                            <div className='category-page'>{this.props.products[this.props.match.params.id - 1].category}</div>
                            <div className="card-title-page">{this.props.products[this.props.match.params.id - 1].title}</div>
                            <div className='description-page'>{this.props.products[this.props.match.params.id - 1].description}</div>
                            <div className="price">Price:- ${this.props.products[this.props.match.params.id - 1].price}</div>
                            <div className="button-container">
                                <button>Buy Now</button>
                                <button >Add To Cart</button>
                            </div>
                        </div>

                    </div>}
            </>
        )
    }
}

export default ProductPage;
