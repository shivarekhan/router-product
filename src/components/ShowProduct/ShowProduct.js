import Card from "../Card/Card";
import Loader from "../Loader/Loader";
import "./ShowProduct.css"


import React from 'react'

function ShowProduct(props) {
    let { fetchedProducts, status, errorMsg } = props;
    return (
        <>
            {status === "loading" && <Loader />}
            {errorMsg !== "" && <h1>{errorMsg}</h1>}
            {status === "loaded" && fetchedProducts.length === 0 && <div className="error-msg">
                <h3>No products available at the moment. Please try again later.</h3>
            </div>}

            {status === "loaded" && <div className="card-parent">
                {fetchedProducts.map((current) => {
                    return (
                        <Card
                            id={current.id}
                            key={current.id}
                            title={current.title}
                            category={current.category}
                            img={current.image}
                            price={current.price}
                            rate={current.rating.rate}
                            count={current.rating.count}
                        />)
                })}
            </div>}
        </>
    )
}

export default ShowProduct;