import React from "react";
import { Link } from "react-router-dom";
import "./Card.css";

class Card extends React.Component {
    
    render() {
        let { id, title, category, img, price, rate, count } = this.props
        return (
            <>
                <Link to={"/product/" + id}>
                    <div className="Card">
                        <img src={img} />
                        <div className="card-category">{category}</div>
                        <div className="card-title">{title}</div>
                        <div className="rate-count"> Count ({count}) & Rating({rate})</div>
                        <div className="price">Price:- ${price}</div>
                    </div>

                </Link>

            </>
        )
    }
}

export default Card;